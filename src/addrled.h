#pragma once
#include "FastLED.h"

template<uint8_t fastled_pin, uint16_t num_leds> class My_fastled {
    CRGB leds[num_leds];

    public:
    template <typename T> CRGB &operator[](T i) {
        return leds[i];
    }

    My_fastled() {
        FastLED.addLeds<WS2812, fastled_pin, BRG>(leds, num_leds);
        FastLED.setBrightness(10);
        FastLED.setCorrection(TypicalSMD5050);
        FastLED.setTemperature(SodiumVapor);
        FastLED.setDither(0);
    }
};

