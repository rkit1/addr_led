#include <Arduino.h>
#include "addrled.h"
constexpr auto num_leds = 60;
My_fastled<10, num_leds> leds;

void setup() {

}
constexpr auto scale = 30;
void loop() {
  auto seg = num_leds / 3;
  auto mid = seg / 2;
  for (auto offset = 0; offset < num_leds * scale; offset ++) {
    for (auto i = 0; i < num_leds; i++) {
      auto l_pos = i + (offset / scale) % seg;
      int distance = abs(mid - l_pos);
      leds[i] = 0xFF * (seg / 4) / distance;
    } 
    FastLED.delay(1000/60);
  }
}